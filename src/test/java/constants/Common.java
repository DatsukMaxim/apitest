package constants;

public interface Common {
    String pathCopy = "copyright";
    String pathNearStat = "nearest_stations";
    String pathNearSett = "nearest_settlement";
    String pathCarr = "carrier";
    String auth = "Authorization";
}
