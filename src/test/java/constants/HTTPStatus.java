package constants;

public interface HTTPStatus {
    Integer r200 = 200;
    Integer r400 = 400;
    Integer r404 = 404;
}
