package helpers;
import constants.*;
import org.junit.jupiter.api.BeforeEach;

import static io.restassured.RestAssured.*;

public class Settings {
    @BeforeEach
    public void init() {
        baseURI = Config.baseUri;
    }
}
