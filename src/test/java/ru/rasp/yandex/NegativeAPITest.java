package ru.rasp.yandex;

import constants.*;
import helpers.Settings;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static io.restassured.RestAssured.given;
import static org.hamcrest.core.Is.is;

public class NegativeAPITest extends Settings {

    @DisplayName(value = "Негативный тест 'Копирайт Яндекс.Расписаний'")
    @ParameterizedTest(name = "CopyrightTest. (\"{0}\")")
    @CsvSource({
            "jsoN",
            "xm1",
            "qweertty",
            "123",
            "js",
            "1~!@#~@$!"
    })
    public void copyrightTestError404(String format) {
        given()
            .header(Common.auth, Config.api_key)
            .param("format", format)
        .when()
            .get(Common.pathCopy)
        .then()
            .log().all()
            .body("code", is(HTTPStatus.r400))
        .extract().response();
    }

    @DisplayName(value = "Негативный тест 'Ближайший город'")
    @ParameterizedTest(name = "NearestSettlementTest. (\"{0}\", \"{1}\")")
    @CsvSource({
            "-91, -180",
            "91, 180",
            "55, 181",
            "22, -181",
            "-91, -181",
            "91, 181",
            "-91, 12"
    })
    public void nearestSettlementTestError404(Double lat, Double lng) {
        given()
            .contentType(ContentType.JSON)
            .header(Common.auth, Config.api_key)
            .param("lat", lat)
            .param("lng", lng)
        .when()
            .get(Common.pathNearSett)
        .then()
            .log().all()
            .statusCode(HTTPStatus.r400)
        .extract().response();
    }

    @DisplayName("Негативный тест 'Список ближайших станций'")
    @ParameterizedTest(name = "NearestStationTest. (\"{0}\", \"{1}\", \"{2}\")")
    @CsvSource({
            "91, 10, 5",
            "-91, 15, 5",
            "65, -181, 0",
            "66, 181, 15",
            "10, 91, -1",
            "10, 91, 51",
            "91, 181, 51",
            "-91, -181, -1"
    })
    public void nearestStationTestError400(Double lat, Double lng, Double distance) {
        given()
            .contentType(ContentType.JSON)
            .header(Common.auth, Config.api_key)
            .param("lat", lat)
            .param("lng", lng)
            .param("distance", distance)
        .when()
            .get(Common.pathNearStat)
        .then()
            .log().all()
            .statusCode(HTTPStatus.r400)
        .extract().response();
    }

    @DisplayName("Негативный тест 'Информация о перевозчике'")
    @ParameterizedTest(name = "CarrierTest. (\"{0}\")")
    @CsvSource({
            "1!!1",
            "jhdfd",
            "!№;:№;%:",
            "0"
    })
    public void carrierTestError404(String code) {
        given()
            .header(Common.auth, Config.api_key)
            .param("code", code)
        .when()
            .get(Common.pathCarr)
        .then()
            .log().all()
            .statusCode(HTTPStatus.r404)
        .extract().response();
    }
}
