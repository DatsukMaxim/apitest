package ru.rasp.yandex;

import constants.*;
import helpers.Settings;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static io.restassured.RestAssured.*;

public class PositiveAPITest extends Settings {

    @DisplayName(value = "Позитивный тест 'Копирайт Яндекс.Расписаний'")
    @ParameterizedTest(name = "CopyrightTest. (\"{0}\")")
    @CsvSource({
            "json",
            "xml"
    })
    public void copyrightTest(String format) {
        given()
            .header(Common.auth, Config.api_key)
            .param("format", format)
        .when()
            .get(Common.pathCopy)
        .then()
            .log().all()
            .statusCode(HTTPStatus.r200)
        .extract().response();
    }

    @DisplayName("Позитивный тест 'Список ближайших станций'")
    @ParameterizedTest(name = "NearestStationTest. (\"{0}\", \"{1}\", \"{2}\")")
    @CsvSource({
            "50.440046, 40.4882367, 50",
            "56.205741, 86.451366, 0",
            "90, 180, 50",
            "-90, -180, 50",
            "90, -180, 50",
            "-90, 180, 50",
            "-90, 0, 50",
            "90, 56, 50"
    })
    public void nearestStationTest(Double lat, Double lng, Double distance) {
        given()
            .contentType(ContentType.JSON)
            .header(Common.auth, Config.api_key)
            .param("lat", lat)
            .param("lng", lng)
            .param("distance", distance)
        .when()
            .get(Common.pathNearStat)
        .then()
            .log().all()
            .statusCode(HTTPStatus.r200)
        .extract().response();
    }

    @DisplayName("Позитивный тест 'Информация о перевозчике'")
    @ParameterizedTest(name = "CarrierTest. (\"{0}\")")
    @CsvSource({
            "680",
            "100",
            "320",
            "5483"
    })
    public void carrierTest(String code) {
        given()
            .header(Common.auth, Config.api_key)
            .param("code", code)
        .when()
            .get(Common.pathCarr)
        .then()
            .log().all()
            .statusCode(HTTPStatus.r200)
        .extract().response();
    }
}
